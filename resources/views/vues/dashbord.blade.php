@extends("layouts.main")

@section('content')
<div class="col-md-12">
    <div class="card card-plain">
      <div class="card-header card-header-primary">
        <h4 class="card-title mt-0"> Liste des Pays</h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-hover">
            <thead class="">
              <tr><th>
                ID
              </th>
              <th>
                libelle
              </th>
              <th>
                description
              </th>
              <th>
                code indication
              </th>
              <th>
                continent
              </th>
              <th>
                population
              </th>
              <th>
                capitale
              </th>
              <th>
                monnaie
              </th>
              <th>
                langue
              </th>
              <th>
                superficie
              </th>
              <th>
                laïque
              </th>
            </tr></thead>
            <tbody>
                @foreach($pays as $listepays)
                    <tr>
                        <td>
                            {{$listepays->id}}
                        </td>
                        <td>
                            {{$listepays->libelle}}
                        </td>
                        <td>
                            {{$listepays->description}}
                        </td>
                        <td>
                            {{$listepays->code_indicatif}}
                        </td>
                        <td>
                            {{$listepays->continent}}
                        </td>
                        <td>
                            {{$listepays->population}}
                        </td>
                        <td>
                            {{$listepays->capitale}}
                        </td>
                        <td>
                            {{$listepays->monnaie}}
                        </td>
                        <td>
                            {{$listepays->langue}}
                        </td>
                        <td>
                            {{$listepays->superficie}}
                        </td>
                        <td>
                            {{$listepays->est_laique}}
                        </td>
                        <td>
                            <a href="{{url('/')}}" class="btn btn-warning btn-sm" title="Visualiser"><i class="material-icons">visibility</i></a>
                            <a href="{{url('/')}}" class="btn btn-primary btn-sm" title="Modifier"><i class="material-icons">edit</i></a>
                            <a href="{{url('/')}}" class="btn btn-danger btn-sm" title="Supprimer"><i class="material-icons">delete</i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      </div>
      <div class="card-tools">
        <div class="input-group input-group-sm" style="width: 80px;">
            <a href="{{url('/pays/create')}}">
                <button class="btn btn-primary">Ajouter un pays</button>
            </a>
        </div>
    </div>
    </div>
  </div>
@endsection
