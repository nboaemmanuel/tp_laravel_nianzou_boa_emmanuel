@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header card-header-primary">
          <h4 class="card-title">Ajouter un pays</h4>
        </div>
        <div class="card-body">
          <form class="form-horizontal" action="{{route('pays.store')}}" method="POST">
            @method("POST")
            @csrf
            <div class="row">
              <div class="col-md-5">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Libelle</label>
                  <input type="text" name="libelle" class="form-control" required>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Description</label>
                  <input type="text" name="description" class="form-control">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Code indicatif</label>
                  <input type="text" name="code_indicatif" class="form-control">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group bmd-form-group">
                    <select class="form-control" name="continent">
                        <option value = "" disabled selected hidden>Continent</option>
                        <option value="Europe">Europe</option>
                        <option value="Afrique">Afrique</option>
                        <option value="Asie">Asie</option>
                        <option value="Amerique">Amerique</option>
                    </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Population</label>
                  <input type="number" name="population" class="form-control" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Capitale</label>
                  <input type="text" name="capitale" class="form-control" required>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group bmd-form-group">
                    <select class="form-control" name="monnaie" required>
                        <option value = "" disabled selected hidden>Monnaie</option>
                        <option value="XOF">XOF</option>
                        <option value="EUR">EUR</option>
                        <option value="DOLLAR">DOLLAR</option>
                    </select>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <select class="form-control" name="langue" required>
                    <option value = "" disabled selected hidden>Langue</option>
                    <option value="FR">FR</option>
                    <option value="EN">EN</option>
                    <option value="AR">AR</option>
                    <option value="ES">ES</option>
                </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Superficie</label>
                  <input type="number" name="superficie" class="form-control">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <select class="form-control" name="est_laique">
                    <option value = "" disabled selected hidden>Pays laïque ?</option>
                    <option value="1">Oui</option>
                    <option value="2">Non</option>
                </select>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary pull-right">Enregistrer</button>
            <a href=" " class="btn btn-default float-right">Annuler</a>
            <div class="clearfix"></div>
          </form>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card card-profile">
        <div class="card-avatar">
          <a href="javascript:;">
            <img class="img" src="../assets/img/faces/marc.jpg">
          </a>
        </div>
        <div class="card-body">
          <h6 class="card-category text-gray">CEO / Co-Founder</h6>
          <h4 class="card-title">Emmanuel Nianzou</h4>
          <p class="card-description">
            Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
          </p>
          <a href="javascript:;" class="btn btn-primary btn-round">Follow</a>
        </div>
      </div>
    </div>
  </div>
@endsection
