<?php

namespace App\Http\Controllers;

use App\Models\pays;
use Illuminate\Http\Request;

class PaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('vues.dashbord',[
            "pays"=>pays::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vues.ajout');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "libelle"=>'required',
            "capitale"=>'required',
            "monnaie"=>'required',
            "langue"=>'required'
        ]);

        pays::create([
            "libelle"=>$request->get('libelle'),
            "description"=>$request->get('description'),
            "code_indicatif"=>$request->get('code_indicatif'),
            "continent"=>$request->get('continent'),
            "population"=>$request->get('population'),
            "capitale"=>$request->get('capitale'),
            "monnaie"=>$request->get('monnaie'),
            "langue"=>$request->get('langue'),
            "superficie"=>$request->get('superficie'),
            "est_laique"=>$request->get('est_laique')
        ]);

        return redirect()->route("vues.dashbord");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function show(pays $pays)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function edit(pays $pays)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, pays $pays)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function destroy(pays $pays)
    {
        //
    }
}
