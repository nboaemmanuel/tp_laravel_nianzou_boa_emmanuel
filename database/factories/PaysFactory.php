<?php

namespace Database\Factories;

use App\Models\pays;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaysFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = pays::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' =>$this->faker->country,
            'description' =>$this->faker->sentence,
            'code_indicatif' =>$this->faker->unique()->countryCode,
            'continent' =>$this->faker->randomElement(["AFRIQUE", "EUROPE", "ASIE", "AMERIQUE"]),
            'population' =>rand(5000000, 35000000),
            'capitale' =>$this->faker->city,
            'monnaie' =>$this->faker->randomElement(["XOF", "EUR", "DOLLAR"]),
            'langue' =>$this->faker->randomElement(["FR", "EN", "AR", "ES"]),
            'superficie' =>rand(122462, 20000000),
            'est_laique' =>$this->faker->randomElement([true, false]),
        ];
    }
}
